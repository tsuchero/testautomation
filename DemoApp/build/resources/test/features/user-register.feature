Feature: Register as a new user
  As i guest you want to register new account in order to be able to access application functionalities
  ----------------------
  As a guest user
  I want to register new account
  So that I am able to login and ise applications functionalities
  ----------------------
  Business rules:
  (1) User name constraint aka valid email address
  (2) Password policy...

  Scenario: Successful user account registration
    Given I am on the registration page "http://wagner.wang/Identity/Account/Register"
    When I type "username@somedomain.com" in the email field
      And I type "Contoso!1234" in the password field
      And I type "Contoso!1234" in the confirm password field
      And I type "Intership guy" in full name field
      And I enter valid date "mm/dd/yyyy" in birth date field
      And I press the "Register" button
    Then I should be logged and redirected to main page
      And my username should appear on the page header in upper right corner