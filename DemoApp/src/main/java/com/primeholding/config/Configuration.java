package com.primeholding.config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configuration.properties"})
public interface Configuration  extends Config{

    @Key("browser")
    String browser();

    @Key("waitTime")
    int waitTime();

    @Key("path.screenShots")
    String path2Screenshots();

    @Key("path.testData")
    String path2testData();
}
