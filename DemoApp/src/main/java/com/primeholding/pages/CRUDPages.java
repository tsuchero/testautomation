package com.primeholding.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CRUDPages {

    WebDriver driver;


    public CRUDPages(WebDriver driver, int waitTime){
        this.driver = driver;
        PageFactory.initElements(driver, this);


    }

    @FindBy (xpath ="//*[text() ='Create New']")
    private WebElement theCreateButton;
    public WebElement createButton() { return theCreateButton; }

    @FindBy(id = "LastName")
    private WebElement theLastName;
    public WebElement lastName() { return theLastName; }

    @FindBy(id = "FirstMidName")
    private WebElement theFirstName;
    public WebElement firstName() { return theFirstName;}

    @FindBy(id="EnrollmentDate")
    private WebElement theEnrollmentDate;
    public WebElement enrollmentDate() { return theEnrollmentDate; }

    @FindBy (xpath ="(//*[text() = 'Edit'])[1]")
    private WebElement theEditButton;
    public WebElement editButton() { return theEditButton; }

    @FindBy(xpath ="(//*[text() = 'Details'])[1]")
    private WebElement theDetails;
    public WebElement details() { return theDetails; }

    @FindBy(xpath ="//*[text() ='Course Title']")
    private WebElement theCourseTitle;
    public WebElement courseTitle() { return theCourseTitle; }

    @FindBy(xpath ="(//*[text() = 'Delete'])[1]")
    private WebElement theDelete;
    public WebElement delete() { return theDelete; }

    @FindBy(xpath = "//a[@href='/ContosoUniversity/Students']")
    private WebElement theBackButton;
    public WebElement backButton() { return theBackButton; }

}
