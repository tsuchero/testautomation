package com.primeholding.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;


    public LoginPage(WebDriver driver, int waitTime){
        this.driver = driver;
        PageFactory.initElements(driver, this);


    }
    @FindBy(id = "login")
    private WebElement theLogin;
    public WebElement Login() { return theLogin; }

    @FindBy(id= "Input_Email")
    private WebElement theEmail;
    public WebElement Email() { return theEmail; }

    @FindBy(id= "Input_Password")
    private WebElement thePassword;
    public WebElement Password() { return thePassword; }

    @FindBy(id = "logout")
    private WebElement theLogout;
    public WebElement Logout() { return theLogout;}

    @FindBy(xpath = "//*[text() = 'Invalid login attempt.']")
    private WebElement theAlert;
    public WebElement alert() { return theAlert;}


}
