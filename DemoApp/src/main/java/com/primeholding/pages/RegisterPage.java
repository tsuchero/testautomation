package com.primeholding.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {

        WebDriver driver;


        public RegisterPage(WebDriver driver, int waitTime){
            this.driver = driver;
            PageFactory.initElements(driver, this);


        }
        @FindBy(id = "Input_Email")
        private WebElement email;
        public WebElement getEmail() {
                return email;
        }

        @FindBy(id = "Input_Password")
        private WebElement password;
        public WebElement getThePassword() {
                return password;
        }

        @FindBy(id = "Input_ConfirmPassword")
        private WebElement confirmPassword;
        public WebElement getConfirmPassword() {
                return confirmPassword;
        }

        @FindBy(id = "Input_Name")
        private WebElement inputName;
        public WebElement fullName() { return inputName; }

        @FindBy(id = "Input_DOB")
        private WebElement dateOfBirth;
        public WebElement DOB() { return dateOfBirth; }

        @FindBy(id = "logout")
        private WebElement theLogout;
        public WebElement Logout() { return theLogout; }

}
