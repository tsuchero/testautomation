package com.primeholding;
import com.primeholding.pages.CRUDPages;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class CRUDStudentsTests extends TestBase {

    CRUDPages crudPages;
    String url = "http://wagner.wang/ContosoUniversity/Students";

    @Test (dataProvider = "CreateStudent",
            dataProviderClass = DataProviderCRUD.class,
            enabled = true,
            priority = 1,
            description = "Create student")
    public void createStudent(String firstName, String lastName, String date) {
        crudPages = new CRUDPages(driver, waitTime);
        driver.get(url);
        crudPages.createButton().click();
        crudPages.lastName().sendKeys(lastName);

        crudPages.firstName().sendKeys(firstName);
        crudPages.enrollmentDate().sendKeys(date);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        crudPages.enrollmentDate().submit();
        if (crudPages.createButton().isDisplayed()) {
            System.out.println("Successfully created student");
        } else {
            System.out.println("Unsuccessfully created student");
        }
    }

    @Test(dataProvider = "dontCreateStudent",
            dataProviderClass = DataProviderCRUD.class,
            enabled = true,
            priority = 1,
            description = "Invalid student creation")
    public void dontCreateStudent(String lastName, String date) {
        crudPages = new CRUDPages(driver, waitTime);
        driver.get(url);
        crudPages.createButton().click();
        // Do not fill firstName
        crudPages.lastName().sendKeys(lastName);

        crudPages.enrollmentDate().sendKeys(date);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        crudPages.enrollmentDate().submit();

        if (crudPages.lastName().isDisplayed()) {
            System.out.println("Student creation is unsuccessful (passed test)");
        } else {
            System.out.println("Student creation is successful (failed test)");
        }
    }

    @Test(enabled = true)
    public void editStudent() {
        crudPages = new CRUDPages(driver, waitTime);
        driver.get(url);
        crudPages.editButton().click();
        crudPages.lastName().clear();
        crudPages.lastName().sendKeys("TestName");
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        crudPages.lastName().submit();
        if (crudPages.createButton().isDisplayed()) {
            System.out.println("Successfully edited student");
        } else {
            System.out.println("Unsuccessfully edited student");
        }
    }
        @Test(enabled = true)
        public void studentDetails () {
            crudPages = new CRUDPages(driver, waitTime);
            driver.get(url);
            crudPages.details().click();
            if (crudPages.courseTitle().isDisplayed()) {
                System.out.println("Successfully opened student details");
            } else {
                System.out.println("Unsuccessfully opened student details");
            }
        }
    @Test(enabled = true)
    public void deleteStudent() {
        crudPages = new CRUDPages(driver, waitTime);
        driver.get(url);
        crudPages.delete().click();
        driver.findElement(By.cssSelector("input[value='Delete']")).submit();

        if (crudPages.createButton().isDisplayed()) {
            System.out.println("Successfully deleted student");
        } else {
            System.out.println("Unsuccessfully deleted student");
        }
    }
    }


