package com.primeholding;

import org.testng.annotations.DataProvider;

public class DataProviderCRUD {
    @DataProvider(name = "CreateStudent")
    public static Object[][] getData() {
        Object[][] data = {{"Michael", "Schumacher", "03/23/2021"},
                {"Ludwig van", "Bethoven", "03/23/2021"}};
        return data;
    }

    @DataProvider(name = "dontCreateStudent")
    public static Object[][] getTheData() {
        Object[][] data = {{"Schumacher", "03/23/2021"},
                {"Bethoven", "03/23/2021"}};
        return data;
    }
}
