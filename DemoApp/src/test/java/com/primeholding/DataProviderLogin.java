package com.primeholding;
import org.testng.annotations.DataProvider;


public class DataProviderLogin {


    @DataProvider(name = "loginDP")
        public static Object[][] getTheData() {
            Object[][] data = {{"penchotesta@abv.bg", "blabla"},
                    {"penchotesta1@abv.bg", "asdasd"}};
            return data;
        }
    @DataProvider(name = "loginDPInvalid")
    public static Object[][] getTheInvalidData() {
        Object[][] data = {{"penchotesta2@abv.bg", "blabla"},
                {"penchotesta3@abv.bg", "asdasd"}};
        return data;
    }
    }

