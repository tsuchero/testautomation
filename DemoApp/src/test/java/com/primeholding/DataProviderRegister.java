package com.primeholding;

import org.testng.annotations.DataProvider;

import java.util.Random;

public class DataProviderRegister {

Random character = new Random();
int characters = character.nextInt(999);
    @DataProvider(name = "registerDP")
    public Object[][] getTheData() {
        Object[][] data = {{"mitko"+characters+"@abv.bg", "tainaparola", "tainaparola", "Mitko Kalaidjiev", "19/03/1936"},
                {"pencho"+characters+"@abv.bg", "tainaparola", "tainaparola", "Pencho Slaveikov","19/03/1963"}};
        return data;
    }

    @DataProvider(name = "registerDP1")
    public static Object[][] getTheInvalidData() {
        Object[][] data1 = {{"tainaparola", "tainaparola", "Mitko Kalaidjiev", "19/03/1936"},
                {"tainaparola", "tainaparola", "Pencho Slaveikov","19/03/1963"}};
        return data1;
    }
}