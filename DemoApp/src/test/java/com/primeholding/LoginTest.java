package com.primeholding;
import com.primeholding.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class LoginTest extends TestBase {

    LoginPage loginPage;
    String url = "http://wagner.wang/Identity/Account/Login";

    @Test(dataProvider = "loginDP",
            dataProviderClass = DataProviderLogin.class,
            enabled = true,
            priority = 1,
            description = "Login successfully")
    public void loginHappyPath(String email, String password){
        loginPage = new LoginPage(driver, waitTime);
        driver.get(url);
        loginPage.Email().click();
        loginPage.Email().sendKeys(email);

        loginPage.Password().click();
        loginPage.Password().sendKeys(password);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }

        loginPage.Password().submit();

        if (loginPage.Logout().isDisplayed()) {
            System.out.println("Login successful");
        } else {
            System.out.println("Login successful");
        }
    }
    @Test(dataProvider = "loginDPInvalid",
            dataProviderClass = DataProviderLogin.class,
            enabled = true,
            priority = 1,
            description = "Login unsuccessfully")
    public void loginNegative(String email, String password){
        loginPage = new LoginPage(driver, waitTime);
        driver.get(url);
        loginPage.Email().click();
        loginPage.Email().sendKeys(email);

        loginPage.Password().click();
        loginPage.Password().sendKeys(password);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        loginPage.Password().submit();
        boolean alertPresence = loginPage.alert().isDisplayed();
        Assert.assertTrue(alertPresence);

        if (loginPage.alert().isDisplayed()) {
            System.out.println("Invalid login test passed");
        } else {
            System.out.println("Invalid login test failed");
        }
    }
}


