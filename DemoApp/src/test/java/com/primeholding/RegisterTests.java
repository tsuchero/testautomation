package com.primeholding;
import com.primeholding.pages.RegisterPage;
import org.testng.annotations.Test;

public class RegisterTests extends TestBase {

    RegisterPage registerPage;
    String url = "http://wagner.wang/Identity/Account/Register";

    @Test(dataProvider = "registerDP",
            dataProviderClass = DataProviderRegister.class,
            enabled = true,
            priority = 1,
            description = "Registration happy path")
    public void testRegisterHappyPath(String theEmail, String thePassword, String passwordAgain, String fullName, String DOB) {
        registerPage = new RegisterPage(driver, waitTime);
        driver.get(url);
        registerPage.getEmail().click();
        registerPage.getEmail().sendKeys(theEmail);

        registerPage.getThePassword().click();
        registerPage.getThePassword().sendKeys(thePassword);

        registerPage.getConfirmPassword().click();
        registerPage.getConfirmPassword().sendKeys(passwordAgain);

        registerPage.fullName().click();
        registerPage.fullName().sendKeys(fullName);

        registerPage.DOB().sendKeys(DOB);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        registerPage.DOB().submit();

        if (registerPage.Logout().isDisplayed()) {
            System.out.println("Registration test is passed");
        } else {
            System.out.println("Registration test is failed");
        }




    }
    @Test(dataProvider = "registerDP1",
            dataProviderClass = DataProviderRegister.class,
            enabled = true,
            priority = 2,
            description = "Registration invalid path")
    public void testRegisterNoEmail(String thePassword, String passwordAgain, String fullName, String DOB) {
        registerPage = new RegisterPage(driver, waitTime);
        driver.get(url);

        registerPage.getThePassword().click();
        registerPage.getThePassword().sendKeys(thePassword);

        registerPage.getConfirmPassword().click();
        registerPage.getConfirmPassword().sendKeys(passwordAgain);

        registerPage.fullName().click();
        registerPage.fullName().sendKeys(fullName);

        registerPage.DOB().sendKeys(DOB);
        if(DEMO){ try { Thread.sleep(demoWait); } catch (InterruptedException e) { e.printStackTrace(); } }
        registerPage.DOB().submit();

        if (registerPage.getEmail().isDisplayed()) {
            System.out.println("Unsuccessful registration test is passed");
        } else {
            System.out.println("Unsuccessful registration test is failed");
        }
    }
}

