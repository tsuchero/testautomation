package com.primeholding;

import com.primeholding.config.ConfigurationManager;
import com.primeholding.driver.DriverType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;

public abstract class TestBase {

    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss");

    protected static final boolean DEMO = true;
    protected static int demoWait = 2000;

    protected static WebDriver driver;
    protected static WebDriverWait wait;
    protected int waitTime = 0;

    @BeforeSuite(alwaysRun = true)
    public void suiteSetup(){
        String browserName = ConfigurationManager.configuration().browser();
        DriverType browser= DriverType.getDriverByName(browserName);
        switch (browser){
            case CHROME:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                /* todo */
                break;
            case EDGE:
                /* todo */
                break;
            case IE:
                /* SKIP :) */
                break;
        }
        //driver = driverManager.getDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(ConfigurationManager.configuration().waitTime()));
        waitTime = ConfigurationManager.configuration().waitTime();
    }

    // @AfterSuite(alwaysRun = true)
    //public void suiteTearDown(){
        //if(driver!=null){
            //driver.quit();
        //}
    //}

    @AfterMethod(alwaysRun = true)
    public void methodPostConditions(ITestResult testResult) throws IOException {

        if(testResult.getStatus() == ITestResult.FAILURE){
            takeScreenshot(testResult.getName(), Arrays.toString(testResult.getParameters()));
        }

    }

    public static void takeScreenshot(String testMethod, String testParams){
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(ConfigurationManager.configuration().path2Screenshots() + testMethod + "-"
                    + testParams  +  sdf.format(new Timestamp(System.currentTimeMillis()))  + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
