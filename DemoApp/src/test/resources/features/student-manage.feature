Feature: Manage (CRUD) students data
  Logged in user should be able to manage (create, read update and delete) data about students in the course
  -------------------
  As a logged user
  I want to manage data about students in the course
  So that I can keep the track of the students enrolled
  -------------------

  Scenario: Successful creation of new student (record)
    Given And I am logged in user
      And I'm on the "http://wagner.wang/ContosoUniversity/Students" page
      When I click on "Create New" link
      And I enter last name in "Last Name" field
      And I enter first name in "First Name" field
      And I enter date in "Enrollment Date" field
      And I click on "Create" button
    Then new student record will be created and I'll be redirected to student page
      And I can see that student is created by finding student with search

  Scenario: Successful reading of existing student's data
     Given And I am logged in user
       And I'm on the "http://wagner.wang/ContosoUniversity/Students" page
       When I click "Details" link on the chosen student
     Then the "Details" page opens
       And I can see the student's Details



  Scenario: Successful editing of existing student
  Given And I am logged in user
        And I'm on the "http://wagner.wang/ContosoUniversity/Students" page
        When I click on "Edit" link on the chosen student
        And I enter last name in "Last Name" field
        And I enter first name in "First Name" field
        And I enter date in "Enrollment Date" field
        And I click on "Save" button
      Then existing student record will be edited and I'll be redirected to students page
        And I can see that student is edited by finding student with search

  Scenario: Successful (record) deletion of existing student
  Given And I am logged in user
          And I'm on the "http://wagner.wang/ContosoUniversity/Students" page
          When I click on "Delete" link on the chosen student
          And I click "Delete" button
        Then existing student record will be deleted and I'll be redirected to students page
          And I can see that student is deleted by trying to find the student with search